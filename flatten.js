function flatten(elements = [], depth = 1, countDepth = 1, array = [], end = [], depthend) {
    if (Array.isArray(elements)) {

        for (let index = 0; index < elements.length; index++) {
            if (Array.isArray(elements[index])) {
                if (depth != 0) {

                    if (countDepth <= depth) {
                        try {
                            return flatten(elements[index], depth, countDepth + 1, array, end = end.concat(elements.slice(index + 1)), depthend = countDepth)
                        }
                        catch {
                            return flatten(elements[index], depth, countDepth + 1, array, [], depthend = countDepth)

                        }
                    }
                }

            }
            if (elements[index] != null) {
                array.push(elements[index])
            }
        }
        if (end.length == 0) {
            return array
        }
        else {
            return flatten(end, depth, 1, array, [], 0)
        }
    }
}


module.exports = flatten

