function filter(elements = [], cb) {
    if (Array.isArray(elements) && (typeof cb == 'function')) {
        let returnArray = []
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements) == true) {
                returnArray.push(elements[index])
            }
        }
        return returnArray

    }
}
module.exports = filter