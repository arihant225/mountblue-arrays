const reduce = require("../reduce");
let items = [1, 2, 3, 4, 5, 5]



//you can call this by function method


/*
let result = reduce(items, function (accumulator, ele) {
    return accumulator = accumulator + ele
})


console.log(result)
*/


//you can call this by arrow method


/*
let result = reduce(items,(accumulator, ele)=> {
    return accumulator = accumulator + ele
},10)

console.log(result)
*/