const filter = require('../filter')
const items = [1, 2, 3, 4, 5, 5]



//you can call it with function method

/*
data=filter(items,function(ele){
    return ele%2==0
})
console.log(data)
*/


// can call it with arrow method

/*
data=filter(items,(ele)=>{
    return ele%2!=0
})
console.log(data)
*/