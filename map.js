
function map(elements = [], cb) {
    if ((Array.isArray(elements)) && (typeof cb == 'function')) {
        let returnArray = []
        for (let index = 0; index < elements.length; index++) {
            let data = cb(elements[index], index, elements)
            returnArray.push(data)
        }
        return returnArray

    }
}

module.exports = map